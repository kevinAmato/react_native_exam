import {View, Text, Rating, ImageBackground, StyleSheet} from 'react-native';
import React from 'react';
import Colors from '../Colors';
import GlobalStyles from '../GlobalStyles';

const ProductDetails = produit => {
  // eslint-disable-next-line prettier/prettier
  const { route: { params }} = produit;
  return (
    <View style={[styles.card, GlobalStyles.shadow]}>
      <View style={styles.titleCard}>
        <Text>{params.title}</Text>
      </View>
      <View style={styles.contentCard}>
        {/* <Rating startingValue={params.rating / 2} readOnly imageSize={15} /> */}
        <Text> {params.overview} </Text>
        <View style={styles.date}>
          <Text>Date de sortie {params.release_date}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  date: {
    paddingTop: 20,
    fontSize: 20,
    textDecorationStyle: 'solid',
  },
  descriptionCard: {
    fontFamily: 'Comfortaa Medium',
    fontSize: 16,
    display: 'flex',
    flexDirection: 'column',
  },
  contentCard: {
    padding: 5,
    flexShrink: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  titleCard: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Comfortaa',
  },
  card: {
    margin: 10,
    height: 'auto',
    borderRadius: 5,
    backgroundColor: Colors.white,
  },
});

export default ProductDetails;
