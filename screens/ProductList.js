import {ScrollView, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import CardProduct from '../components/CardProduct';
import {useEffect} from 'react';

const ProductList = ({navigation}) => {
  const [listeProduit, setListeProduit] = useState([]);

  useEffect(() => {
    fetch(
      'https://api.themoviedb.org/3/movie/popular?api_key=ae90833ceb596238290cb29f7399317d&language=fr-FR',
    )
      .then(json => json.json())
      .then(resultat => {
        setListeProduit(resultat.results);
      })
      .catch(function (error) {
        console.log(
          'There has been a problem with your fetch operation: ' +
            error.message,
        );
      });
  }, []);

  return (
    <ScrollView contentInsetAdjustmentBehavior="automatic">
      {listeProduit.map(produit => (
        <TouchableOpacity
          key={produit.id}
          onPress={() => navigation.navigate('Product details', produit)}>
          <CardProduct
            title={produit.title}
            thumbnail={`https://image.tmdb.org/t/p/w500${produit.poster_path}`}
            description={produit.overview}
            rating={produit.vote_average}
            popularity={produit.popularity}
          />
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
};

export default ProductList;
